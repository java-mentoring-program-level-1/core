package com.epam.jmp.storage;

import java.util.HashMap;
import java.util.Map;

import com.epam.jmp.model.User;

public class BookingStorage {
	private final Map<String, Object> storage = new HashMap<>();
	
	public void put(String key, Object value) {
		storage.put(key, value);
	}
	
	public void addAll(Map<String, Object> objects) {
		storage.putAll(objects);
	}

	public Map<String, Object> getStorage() {
		return storage;
	}

	public void clean() {
		this.storage.clear();
	}

	public Object get(String key) {
		return storage.get(key);
	}

	public Object remove(String key) {
		return storage.remove(key);
	}
}
