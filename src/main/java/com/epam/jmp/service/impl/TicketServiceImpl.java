package com.epam.jmp.service.impl;

import java.util.List;

import com.epam.jmp.model.Event;
import com.epam.jmp.model.Ticket;
import com.epam.jmp.model.Ticket.Category;
import com.epam.jmp.model.User;
import com.epam.jmp.model.exception.EntityNotFoundException;
import com.epam.jmp.repository.EventRepository;
import com.epam.jmp.repository.TicketRepository;
import com.epam.jmp.repository.UserRepository;
import com.epam.jmp.service.TicketService;

public class TicketServiceImpl implements TicketService {
	private TicketRepository ticketRepository;
	private EventRepository eventRepository;
	private UserRepository userRepository;
	
	public TicketServiceImpl(TicketRepository ticketRepository, EventRepository eventRepository, UserRepository userRepository) {
		this.ticketRepository = ticketRepository;
		this.eventRepository = eventRepository;
		this.userRepository = userRepository;
	}

	@Override
	public List<Ticket> getBookedTicket(User user, int pageSize, int pageNumber) {
		return ticketRepository.getBookedTickets(user, pageSize, pageNumber);
	}

	@Override
	public List<Ticket> getBookedTicket(Event event, int pageSize, int pageNumber) {
		return ticketRepository.getBookedTickets(event, pageSize, pageNumber);
	}

	@Override
	public Ticket bookTicket(long userId, long eventId, int place, Category category) {
		boolean isUserPresent = userRepository.isPresent(userId);
		
		if(!isUserPresent) {
			throw new EntityNotFoundException("User with ID: " + userId + " not found");
		}
		
		boolean isEventPresent = eventRepository.isPresent(eventId);
		
		if(!isEventPresent) {
			throw new EntityNotFoundException("Event with ID: " + eventId + " not found");
		}
		return ticketRepository.bookTicket(userId, eventId, place, category);
	}

	@Override
	public boolean cancelTicket(long ticketId) {
		return ticketRepository.cancelTicket(ticketId);
	}

}
