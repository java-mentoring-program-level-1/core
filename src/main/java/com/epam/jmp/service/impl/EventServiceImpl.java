package com.epam.jmp.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.epam.jmp.model.Event;
import com.epam.jmp.repository.EventRepository;
import com.epam.jmp.service.EventService;

public class EventServiceImpl implements EventService {
	private EventRepository repository;

	@Override
	public Event createEvent(Event event) {
		return repository.createEvent(event);
	}

	@Override
	public Optional<Event> getEventById(long eventId) {
		return repository.getEventById(eventId);
	}

	@Override
	public Event updateEvent(Event event) {
		return repository.updateEvent(event);
	}

	@Override
	public boolean deleteEventById(long eventId) {
		return repository.deleteEventById(eventId);
	}

	@Override
	public List<Event> getEventsByTitle(String title, int pageSize, int pageNumber) {
		return repository.getEventsByTitle(title, pageSize, pageNumber);
	}

	@Override
	public List<Event> getEventsForDay(Date day, int pageSize, int pageNumber) {
		return repository.getEventsForDay(day, pageSize, pageNumber);
	}

	public EventRepository getRepository() {
		return repository;
	}
	
	public void setRepository(EventRepository repository) {
		this.repository = repository;
	}
	
	
}
