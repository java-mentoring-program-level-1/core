package com.epam.jmp.service.impl;

import java.util.List;
import java.util.Optional;

import com.epam.jmp.model.User;
import com.epam.jmp.repository.UserRepository;
import com.epam.jmp.service.UserService;

public class UserServiceImpl implements UserService {
	private UserRepository userRepository;
	
	public User createUser(User user) {
		return userRepository.createUser(user);
	}

	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public Optional<User> getUserById(long userId) {
		return userRepository.getUserById(userId);
	}

	@Override
	public User updateUser(User user) {
		return userRepository.updateUser(user);
	}

	@Override
	public boolean deleteUserById(long userId) {
		return userRepository.deleteUserById(userId);
	}

	@Override
	public List<User> getUsersByName(String name, int pageSize, int pageNumber) {
		return userRepository.getUsersByName(name, pageSize, pageNumber);
	}

	@Override
	public Optional<User> getUserByEmail(String email) {
		return userRepository.getUserByEmail(email);
	}

	public UserRepository getUserRepository() {
		return userRepository;
	}
	
}
