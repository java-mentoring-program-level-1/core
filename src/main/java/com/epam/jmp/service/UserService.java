package com.epam.jmp.service;

import java.util.List;
import java.util.Optional;

import com.epam.jmp.model.User;

public interface UserService {
	User createUser(User user);
	Optional<User> getUserById(long userId);
	User updateUser(User user);
	boolean deleteUserById(long userId);
	List<User> getUsersByName(String name, int pageSize, int pageNumber);
	Optional<User> getUserByEmail(String email);
	
}
