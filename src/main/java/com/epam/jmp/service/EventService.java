package com.epam.jmp.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.epam.jmp.model.Event;

public interface EventService {
	Event createEvent(Event event);
	Optional<Event> getEventById(long eventId);
	Event updateEvent(Event event);
	boolean deleteEventById(long eventId);
	List<Event> getEventsByTitle(String title, int pageSize, int pageNumber);
	List<Event> getEventsForDay(Date day, int pageSize, int pageNumber);
}
