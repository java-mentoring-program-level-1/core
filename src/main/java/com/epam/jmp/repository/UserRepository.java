package com.epam.jmp.repository;

import java.util.List;
import java.util.Optional;

import com.epam.jmp.model.User;

public interface UserRepository {
	User createUser(User user);
	Optional<User> getUserById(long userId);
	Optional<User> getUserByEmail(String email);
	User updateUser(User user);
	boolean deleteUserById(long userId);
	List<User> getUsersByName(String name, int pageSize, int pageNumber);
	boolean isPresent(long userId);
}
