package com.epam.jmp.repository;

import java.util.List;

import com.epam.jmp.model.Event;
import com.epam.jmp.model.Ticket;
import com.epam.jmp.model.Ticket.Category;
import com.epam.jmp.model.User;

public interface TicketRepository {
	Ticket bookTicket(long userId, long eventId, int place, Category category);
	boolean cancelTicket(long ticketId);
	List<Ticket> getBookedTickets(User user, int pageSize, int pageNumber);
	List<Ticket> getBookedTickets(Event event, int pageSize, int pageNumber);
	
}
