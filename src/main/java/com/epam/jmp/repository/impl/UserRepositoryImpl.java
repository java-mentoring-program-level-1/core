package com.epam.jmp.repository.impl;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.jmp.model.User;
import com.epam.jmp.repository.UserRepository;
import com.epam.jmp.storage.BookingStorage;

public class UserRepositoryImpl implements UserRepository {
	Logger log = LoggerFactory.getLogger(UserRepositoryImpl.class);
	private final BookingStorage bookingStorage;
	
	public UserRepositoryImpl(BookingStorage bookingStorage) {
		this.bookingStorage = bookingStorage;
	}

	public User createUser(User user) {
		if(Objects.isNull(user.getEmail()) || user.getEmail().length() == 0) {
			throw new IllegalArgumentException("User's email can not be null or empty");
		}
		
		boolean isEmailExist = getUsers().values()
				.stream()
				.anyMatch(u -> u.getEmail().equalsIgnoreCase(user.getEmail()));
		
		if(isEmailExist) {
			throw new IllegalArgumentException("User's email was already used");
		}
		
		Optional<Entry<String, Object>> userOptional = bookingStorage.getStorage().entrySet().stream()
			.filter(p -> {
				if(p.getKey().contains("user"))
					return true;
				return false;
			})
			.max((entry1, entry2) -> Long.compare(((User)entry1.getValue()).getId(), ((User)entry2.getValue()).getId()));
		
		if(userOptional.isPresent()) {
			user.setId(((User)userOptional.get().getValue()).getId() + 1);
		} else {
			user.setId(1L);
		}
		
		bookingStorage.put("user:" + user.getId(), user);
		log.info("Inserted a new user: ID: {}, name: {}, email: {}", user.getId(), user.getName(), user.getEmail());
		return user;
	}

	@Override
	public Optional<User> getUserById(long userId) {
		return Optional.ofNullable((User)bookingStorage.get("user:" + userId));
	}

	@Override
	public Optional<User> getUserByEmail(String email) {
		Optional<Entry<String, User>> userOptional = getUsers().entrySet()
				.stream()
				.filter(entry -> entry.getValue().getEmail() == email ? true : false).findFirst();

		if(userOptional.isPresent()) {
			User user = userOptional.get().getValue();
			log.info("Found an user: ID: {}, name: {}, email: {}", user.getId(), user.getName(), user.getEmail());
			return Optional.of(user);
		}

		return Optional.empty();
	}

	@Override
	public User updateUser(User user) {
		if(Objects.isNull(user)) {
			throw new IllegalArgumentException("User can not be null");
		}

		if(user.getId() <= 0) {
			throw new IllegalArgumentException("User contains an invalid id");
		}
		
		if(Objects.isNull(user.getEmail()) || user.getEmail().trim().length() == 0) {
			throw new IllegalArgumentException("User contains null or invalid email");
		}
		
		boolean isEmailNotValid = getUsers().values().stream().filter(u -> u.getId() != user.getId())
				.anyMatch(exactUser -> exactUser.getEmail().equalsIgnoreCase(user.getEmail()));
		
		if(isEmailNotValid) {
			throw new IllegalArgumentException("User's email was already used");
		}
		
		bookingStorage.put("user:" + user.getId(), user);
		log.info("Update an user: ID: {}, name: {}, email: {}", user.getId(), user.getName(), user.getEmail());
		return user;
	}

	@Override
	public boolean deleteUserById(long userId) {
		User user = (User)bookingStorage.remove("user:" + userId);
		if(user != null) {
			log.info("Deleted an user: ID: {}, name: {}, email: {}", user.getId(), user.getName(), user.getEmail());
			return true;
		}
		return false;
	}

	@Override
	public List<User> getUsersByName(String name, int pageSize, int pageNumber) {
		int skip = (pageNumber - 1) * pageSize;
		return getUsers().values().stream()
				.filter(user -> user.getName().contains(name))
				.skip(skip)
				.limit(pageSize)
				.collect(Collectors.toList());
	}
	
	private Map<String, User> getUsers() {
		return bookingStorage.getStorage().entrySet().stream()
				.filter(p -> {
					if(p.getKey().contains("user"))
						return true;
					return false;
				})
				.collect(Collectors.toMap(p-> p.getKey(), p -> (User)p.getValue()));
	}

	@Override
	public boolean isPresent(long userId) {
		return this.bookingStorage.getStorage().containsKey("user:" + userId);
	}
}
