package com.epam.jmp.repository.impl;

import java.util.Objects;
import java.util.Optional;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.jmp.model.Event;
import com.epam.jmp.model.User;
import com.epam.jmp.repository.EventRepository;
import com.epam.jmp.storage.BookingStorage;

public class EventRepositoryImpl implements EventRepository {
	Logger log = LoggerFactory.getLogger(EventRepositoryImpl.class);
	private BookingStorage storage;
	private SimpleDateFormat formatter;

	public EventRepositoryImpl(BookingStorage storage, SimpleDateFormat formatter) {
		this.storage = storage;
		this.formatter = formatter;
	}

	@Override
	public Event createEvent(Event event) {
		if(Objects.isNull(event.getDate()) || event.getDate().before(new Date())) {
			throw new IllegalArgumentException("Event's date invalid");
		}
		
		if(Objects.isNull(event.getTitle()) || event.getTitle().trim().length() == 0) {
			throw new IllegalArgumentException("Event's title can not be null or empty");
		}
		
		Optional<Entry<String, Object>> eventOptional = storage.getStorage().entrySet().stream()
				.filter(p -> {
					if(p.getKey().contains("event"))
						return true;
					return false;
				})
				.max((entry1, entry2) -> Long.compare(((Event)entry1.getValue()).getId(), ((Event)entry2.getValue()).getId()));
		
		if(eventOptional.isPresent()) {
			event.setId(((Event)eventOptional.get().getValue()).getId() + 1);
		} else {
			event.setId(1L);
		}
		
		storage.put("event:" + event.getId(), event);
		log.info("Inserted a new user into datasource. ID: {}, date: {}, title: {}", event.getId(), event.getDate(), event.getTitle());
		return event;
	}

	@Override
	public Optional<Event> getEventById(long eventId) {
		return Optional.ofNullable((Event)storage.get("event:" + eventId));
	}

	@Override
	public Event updateEvent(Event event) {
		if(Objects.isNull(event)) {
			throw new IllegalArgumentException("Event can not be null");
		}

		if(event.getId() <= 0) {
			throw new IllegalArgumentException("Event contains an invalid id");
		}
		
		if(Objects.isNull(event.getTitle()) || event.getTitle().trim().length() == 0) {
			throw new IllegalArgumentException("Event contains null or invalid email");
		}
		
		if(Objects.isNull(event.getDate())) {
			throw new IllegalArgumentException("Event contains null date");
		}
		
		storage.put("event:" + event.getId(), event);
		log.info("Updated an event. ID: {}, date: {}, title: {}", event.getId(), event.getDate(), event.getTitle());
		return event;
	}

	@Override
	public boolean deleteEventById(long eventId) {
		Event event = (Event)storage.remove("event:" + eventId);
		if(event != null) {
			log.info("Deleted an event. ID: {}, date: {}, title: {}", event.getId(), event.getDate(), event.getTitle());
			return true;
		}
		return  false;
	}

	@Override
	public List<Event> getEventsByTitle(String title, int pageSize, int pageNumber) {
		int skip = (pageNumber - 1) * pageSize;
		return getEvents().values()
				.stream()
				.filter(event -> event.getTitle().contains(title))
				.skip(skip)
				.limit(pageSize)
				.collect(Collectors.toList());
	}

	@Override
	public List<Event> getEventsForDay(Date day, int pageSize, int pageNumber) {
		if(Objects.isNull(day)) {
			throw new IllegalArgumentException("Day parameter can not be null");
		}
		
		int skip = (pageNumber - 1) * pageSize;
		return getEvents().values().stream()
				.filter(event -> {
					String eventDateText = formatter.format(event.getDate());
					String dayStringText = formatter.format(day);
					if(eventDateText.contains(dayStringText)) {
						return true;
					}
					return false;
				})
				.skip(skip)
				.limit(pageSize)
				.collect(Collectors.toList());
	}
	
	private Map<String, Event> getEvents() {
		return storage.getStorage().entrySet().stream()
				.filter(p -> {
					if(p.getKey().contains("event"))
						return true;
					return false;
				})
				.collect(Collectors.toMap(p-> p.getKey(), p -> (Event)p.getValue()));
	}

	@Override
	public boolean isPresent(long eventId) {
		return this.storage.getStorage().containsKey("event:" + eventId);
	}
	
}
