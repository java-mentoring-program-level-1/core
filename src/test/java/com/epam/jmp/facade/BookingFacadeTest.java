package com.epam.jmp.facade;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.epam.jmp.facade.impl.BookingFacadeImpl;
import com.epam.jmp.model.Event;
import com.epam.jmp.model.User;
import com.epam.jmp.model.exception.EntityNotFoundException;
import com.epam.jmp.model.impl.EventImpl;
import com.epam.jmp.model.impl.UserImpl;
import com.epam.jmp.service.UserService;
import com.epam.jmp.service.EventService;
import com.epam.jmp.service.TicketService;

@ExtendWith(MockitoExtension.class)
public class BookingFacadeTest {
	BookingFacade facade;
	
	@Mock
	UserService userService;
	@Mock
	EventService eventService;
	@Mock
	TicketService ticketService;
	
	SimpleDateFormat formatter;
	
	@BeforeEach
	public void init() {
		facade = new BookingFacadeImpl(userService, eventService, ticketService);
		formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
	}
	
	@Test
	public void shouldCreateNewUserWhenCreateUser() {
		User user = new UserImpl();
		user.setId(1L);
		user.setEmail("newUser@email.com");
		user.setName("new user");
		
		when(userService.createUser(any())).thenReturn(user);
		User inputUser = new UserImpl();
		inputUser.setEmail("newUser@email.com");
		user.setName("new user");
		facade.createUser(inputUser);
		
		verify(userService, times(1)).createUser(any());
	}
	
	@Test
	public void shouldThrowExceptionWhenCreateUser() {
		User user = new UserImpl();
		user.setEmail("");
		user.setName("new user");
		
		when(userService.createUser(user)).thenThrow(IllegalArgumentException.class);
		assertThrows(IllegalArgumentException.class, () -> {
			facade.createUser(user);
		});
		
		verify(userService, times(1)).createUser(any());
	}
	
	@Test
	public void shouldGetUserWhenGetUserById() {
		long userId = 1L;
		User user = new UserImpl();
		user.setId(1L);
		user.setName("user one");
		user.setEmail("userOne@email.com");
		
		when(userService.getUserById(userId)).thenReturn(Optional.of(user));
		
		User result = facade.getUserById(userId);
		assertNotNull(result);
		assertEquals(user.getId(), result.getId());
		assertEquals(user.getName(), result.getName());
		verify(userService, times(1)).getUserById(userId);
	}
	
	@Test
	public void shouldThrowExceptionWhenGetUserById() {
		long userId = 1L;
		when(userService.getUserById(userId)).thenReturn(Optional.empty());
		
		assertThrows(EntityNotFoundException.class, () -> {
			facade.getUserById(userId);
		});
		verify(userService, times(1)).getUserById(userId);
	}
	
	@Test
	public void shouldGetUserWhenGetUserByEmail() {
		String email = "newUser@email.com";
		User user = new UserImpl();
		user.setId(1L);
		user.setEmail("newUser@email.com");
		user.setName("new user");
		when(userService.getUserByEmail(email)).thenReturn(Optional.of(user));
		
		User result = facade.getUserByEmail(email);
		assertNotNull(result);
		assertEquals(user.getEmail(), result.getEmail());
		verify(userService, times(1)).getUserByEmail(email);
	}
	
	@Test
	public void shouldThrowExceptionWhenGetUserByEmail() {
		String email = null;
		when(userService.getUserByEmail(email)).thenReturn(Optional.empty());
		
		assertThrows(EntityNotFoundException.class, () -> {facade.getUserByEmail(email);});		
		verify(userService, times(1)).getUserByEmail(email);
	}
	
	@Test
	public void shouldReturnUpdatedUserWhenUpdateUser() {
		User user = new UserImpl();
		user.setId(1L);
		user.setEmail("newUser@email.com");
		user.setName("new User");
		
		User updatedUser = new UserImpl();
		user.setId(1L);
		user.setEmail("updateUser@email.com");
		user.setName("new user");
		
		when(userService.updateUser(user)).thenReturn(updatedUser);
		
		User result = facade.updateUser(user);
		
		assertEquals(updatedUser.getId(), result.getId());
		assertEquals(updatedUser.getEmail(), result.getEmail());
		assertEquals(updatedUser.getName(), result.getName());
	}
	
	@Test
	public void shouldReturnExceptionWhenUpdateUserWithInvalidId() {
		User user = new UserImpl();
		user.setId(0);
		user.setEmail("newUser@email.com");
		user.setName("new User");
		when(userService.updateUser(user)).thenThrow(IllegalArgumentException.class);
		
		assertThrows(IllegalArgumentException.class, ()-> {
			facade.updateUser(user);
		});
	}
	
	@Test
	public void shouldReturnExceptionWhenUpdateUserWithInvalidEmail() {
		User user = new UserImpl();
		user.setId(1L);
		user.setEmail("  ");
		user.setName("new User");
		when(userService.updateUser(user)).thenThrow(IllegalArgumentException.class);
		
		assertThrows(IllegalArgumentException.class, ()-> {
			facade.updateUser(user);
		});
	}
	
	@Test
	public void shouldReturnExceptionWhenUpdateUserWithInvalidUser() {
		User user = null;
		when(userService.updateUser(user)).thenThrow(IllegalArgumentException.class);
		
		assertThrows(IllegalArgumentException.class, ()-> {
			facade.updateUser(user);
		});
	}
	
	@Test
	public void shouldReturnTrueWhenDeleteUser() {
		
		when(userService.deleteUserById(1L)).thenReturn(true);
		boolean result = facade.deleteUser(1L);
		assertTrue(result);
	}
	
	@Test
	public void shouldReturnFalseWhenDeleteUser() {
		
		when(userService.deleteUserById(1L)).thenReturn(false);
		boolean result = userService.deleteUserById(1L);
		assertFalse(result);
	}
	
	@Test
	public void shouldReturnUsersWhenGetUsersByName() {
		User userOne = new UserImpl();
		userOne.setId(1L);
		userOne.setEmail("userOne@email.com");
		userOne.setName("user one");
		
		User userTwo = new UserImpl();
		userTwo.setId(2L);
		userTwo.setEmail("userTwo@email.com");
		userTwo.setName("user two");
		
		String name = "user";
		int pageSize = 5;
		int pageNumber = 1;
		when(userService.getUsersByName(name, pageSize, pageNumber)).thenReturn(List.of(userOne, userTwo));
		
		List<User> users = facade.getUsersByName(name, pageSize, pageNumber);
		
		assertTrue(users.size() == 2);
	}
	
	@Test 
	public void shouldReturnEmptyListWhenGetUsersByName() {
		String name = "new user";
		when(userService.getUsersByName("new user", 5, 2)).thenReturn(Collections.emptyList());
		List<User> users = facade.getUsersByName(name, 5, 2);
		assertTrue(users.size() == 0);
	}
	
	@Test
	public void shouldThrowExceptionWhenGetUsersByName() {
		String name = "name user";
		when(userService.getUsersByName(name, 5, 0)).thenThrow(IllegalArgumentException.class);
		assertThrows(IllegalArgumentException.class, () -> {
			facade.getUsersByName(name, 5, 0);
		});
	}
	
	@Test
	public void shouldReturnNewEventWhenCreateEvent() throws ParseException {
		
		Event newEvent = new EventImpl();
		Date eventDate = formatter.parse("22-01-2022 01:11:12");
		String eventTitle = "Black event";
		newEvent.setId(1L);
		newEvent.setDate(eventDate);
		newEvent.setTitle(eventTitle);
		
		when(eventService.createEvent(any())).thenReturn(newEvent);
		
		Event event = new EventImpl();
		event.setDate(eventDate);
		event.setTitle(eventTitle);
		
		Event result = facade.createEvent(event);
		assertEvent(newEvent, result);
	}
	
	@Test
	public void shouldReturnEventWhenGetEventById() throws ParseException {
		Event event = new EventImpl();
		long eventId = 1L;
		event.setId(eventId);
		event.setDate(formatter.parse("01-12-2021 12:21:22"));
		event.setTitle("Fanta event");
		
		when(eventService.getEventById(eventId)).thenReturn(Optional.of(event));
		
		Event result = facade.getEventById(eventId);
		assertEvent(event, result);
	}
	
	@Test
	public void shouldThrowExceptionEventWhenGetEventById() {
		when(eventService.getEventById(any(Long.class))).thenThrow(EntityNotFoundException.class);
		assertThrows(EntityNotFoundException.class, () -> {
			facade.getEventById(1L);
		});
	}
	
	@Test
	public void shouldReturnUpdatedEventWhenUpdateEvent() throws ParseException {
		Event updatedEvent = new EventImpl();
		updatedEvent.setId(2L);
		updatedEvent.setDate(formatter.parse("03-04-2022 12:00:00"));
		updatedEvent.setTitle("Updated event");
		
		when(eventService.updateEvent(any(Event.class))).thenReturn(updatedEvent);
		
		Event event = new EventImpl();
		event.setId(2L);
		event.setDate(new Date());
		event.setTitle("Black event");
		Event result = facade.updateEvent(event);
		
		assertEvent(updatedEvent, result);
	}
	
	@Test
	public void shouldReturnTrueWhenDeleteEvent() {
		long eventId = 1L;
		when(eventService.deleteEventById(eventId)).thenReturn(true);
		
		boolean result = facade.deleteEvent(eventId);
		assertTrue(result);
	}
	
	@Test
	public void shouldReturnFalseWhenDeleteEvent() {
		long eventId = 2L;
		when(eventService.deleteEventById(eventId)).thenReturn(false);
		
		boolean result = facade.deleteEvent(eventId);
		assertFalse(result);
	}
	
	@Test
	public void shouldReturnEventsWhenGetEventsByTitle() {
		Event event1 = new EventImpl();
		event1.setId(1L);
		event1.setDate(new Date());
		event1.setTitle("evvent one");
		
		Event event2 = new EventImpl();
		event2.setId(2L);
		event2.setDate(new Date());
		event2.setTitle("evvent two");
		
		when(eventService.getEventsByTitle("evvent", 5, 1)).thenReturn(List.of(event1, event2));
		List<Event> events = facade.getEventsByTitle("evvent", 5, 1);
		assertEvent(event1, events.get(0));
		assertEvent(event2, events.get(1));
	}
	
	
	@Test
	public void shouldReturnEmptyListsWhenGetEventsByTitle() {
		when(eventService.getEventsByTitle("no event", 5, 1)).thenReturn(Collections.emptyList());
		List<Event> events = facade.getEventsByTitle("no event", 5, 1);
		assertTrue(events.size() == 0);
	}
	
	@Test
	public void shouldReturnEventsWhenGetEventsForDay() throws ParseException {
		Event event1 = new EventImpl();
		event1.setId(1L);
		event1.setDate(formatter.parse("20-01-2012 15:00:00"));
		event1.setTitle("evvent one");
		
		Event event2 = new EventImpl();
		event2.setId(2L);
		event2.setDate(formatter.parse("20-01-2012 19:00:00"));
		event2.setTitle("evvent two");

		formatter.applyPattern("dd-MM-yyyy");
		Date eventDay = formatter.parse("20-01-2012");
		when(eventService.getEventsForDay(eventDay, 5, 1)).thenReturn(List.of(event1, event2));
		List<Event> events = facade.getEventsForDay(eventDay, 5, 1);
		assertEvent(event1, events.get(0));
		assertEvent(event2, events.get(1));
	}
	
	@Test
	public void shouldReturnEmptyListsWhenGetEventsForDay() throws ParseException {
		formatter.applyPattern("dd-MM-yyyy");
		Date eventDay = formatter.parse("01-01-2018");
		when(eventService.getEventsForDay(eventDay, 5, 1)).thenReturn(Collections.emptyList());
		List<Event> events = facade.getEventsForDay(eventDay, 5, 1);
		assertTrue(events.size() == 0);
	}
	
	private void assertEvent(Event actualEvent, Event expectedEvent) {
		assertEquals(actualEvent.getId(), expectedEvent.getId());
		assertTrue(actualEvent.getDate().toString().equals(expectedEvent.getDate().toString()));
		assertEquals(actualEvent.getTitle(), expectedEvent.getTitle());
	}
}
