package com.epam.jmp.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.epam.jmp.model.User;
import com.epam.jmp.model.impl.UserImpl;
import com.epam.jmp.repository.impl.UserRepositoryImpl;
import com.epam.jmp.storage.BookingStorage;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(value = "/config/daos.xml")
public class UserRepositoryTest {
	
	@Autowired
	UserRepository userRepository;
	
	@Test
	public void shouldReturnNewUserWhenCreateUser() {
		User user = new UserImpl();
		user.setEmail("newUser1@email.com");
		user.setName("new user");
		
		User createdUser = userRepository.createUser(user);
		assertEquals(createdUser.getId(), 6L);
	}
	
	@Test
	public void shouldThrowExceptionWhenCreateUser() {
		User user = new UserImpl();
		
		assertThrows(IllegalArgumentException.class, () -> {
			userRepository.createUser(user);
		});
	}
	
	@Test
	public void shouldThrowExceptionWhenCreateUserWithExistedEmail() {
		User user = new UserImpl();
		user.setEmail("albert_einstein@email.com");
		user.setName("albert einstein");
		
		assertThrows(IllegalArgumentException.class, () -> {
			userRepository.createUser(user);	
		});
		
	}
	
	@Test
	public void shouldReturnUserWhenGetUserById() {
		long userId = 6L;
		Optional<User> user = userRepository.getUserById(userId);
		assertTrue(user.isPresent());
		assertEquals(userId, user.get().getId());
	}
	
	@Test 
	public void shouldReturnEmptyWhenGetUserById() {
		long userId = 20L;
		Optional<User> user = userRepository.getUserById(userId);
		assertFalse(user.isPresent());
	}
	
	@Test
	public void shouldReturnUserWhenGetUserByEmail() {
		String email = "newUser1@email.com";
		Optional<User> user = userRepository.getUserByEmail(email);
		assertTrue(user.isPresent());
		assertEquals(email, user.get().getEmail());
		
	}
	
	@Test
	public void shouldReturnEmptyWhenGetUserByEmail() {
		String email = "otherUser@email.com";
		Optional<User> user = userRepository.getUserByEmail(email);
		assertFalse(user.isPresent());
	}
	
	@Test
	public void shouldReturnUpdatedUserWhenUpdateUser() {
		User user = new UserImpl();
		user.setId(1L);
		user.setEmail("oldUser1@email.com");
		user.setName("old user");
		
		User updatedUser = userRepository.updateUser(user);
		assertEquals(user.getId(), updatedUser.getId());
		assertEquals(user.getEmail(), updatedUser.getEmail());
		assertEquals(user.getName(), updatedUser.getName());
	}
	
	@Test
	public void shouldThrowExceptionWhenUpdateUserWithExistedEmail() {
		User user = new UserImpl();
		user.setId(1L);
		user.setEmail("oldUser1@email.com");
		user.setName("old user");
		
		User updatedUser = userRepository.updateUser(user);
		assertEquals(user.getId(), updatedUser.getId());
		assertEquals(user.getEmail(), updatedUser.getEmail());
		assertEquals(user.getName(), updatedUser.getName());
	}
	
	@Test
	public void shouldThrowExceptionWhenUpdateUserWithNullValue() {
		assertThrows(IllegalArgumentException.class, ()-> {
			userRepository.updateUser(null);
		});
	}
	
	@Test
	public void shouldThrowExceptionWhenUpdateUserWithInvalidId() {
		User user = new UserImpl();
		user.setId(0);
		user.setEmail("seo@email.com");
		user.setName("seo");
		assertThrows(IllegalArgumentException.class, ()-> {
			userRepository.updateUser(user);
		});
	}
	
	@Test
	public void shouldThrowExceptionWhenUpdateUserWithInvalidEmail() {
		User user = new UserImpl();
		user.setId(2L);
		user.setEmail("");
		user.setName("seo");
		assertThrows(IllegalArgumentException.class, ()-> {
			userRepository.updateUser(user);
		});
	}
	
	@Test
	public void shouldReturnTrueWhenDeleteUser() {
		long userId = 1L;
		boolean result = userRepository.deleteUserById(userId);
		assertTrue(result);
	}
	
	@Test
	public void shouldReturnFalseWhenDeleteUser() {
		long userId = 99L;
		boolean result = userRepository.deleteUserById(userId);
		assertFalse(result);
	}

	@Test
	public void shouldReturnUsersWhenGetUsersByName() {
		String name = "new user";
		List<User> users = userRepository.getUsersByName(name, 5, 1);
		assertTrue(users.size() > 0);
	}
	
	@Test 
	public void shouldReturnEmptyListWhenGetUsersByName() {
		String name = "new user";
		List<User> users = userRepository.getUsersByName(name, 5, 2);
		assertTrue(users.size() == 0);
	}
	
	@Test
	public void shouldThrowExceptionWhenGetUsersByName() {
		String name = "name user";
		assertThrows(IllegalArgumentException.class, () -> {
			userRepository.getUsersByName(name, 5, 0);
		});
	}
	
	@Test
	public void shouldReturnTrueWhenIsPresent() {
		boolean result = userRepository.isPresent(2L);
		
		assertTrue(result);
	}
	
	@Test
	public void shouldReturnFalseWhenIsPresent() {
		boolean result = userRepository.isPresent(100L);
		
		assertFalse(result);
	}
}
