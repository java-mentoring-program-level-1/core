package com.epam.jmp.repository;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.epam.jmp.model.Event;
import com.epam.jmp.model.Ticket;
import com.epam.jmp.model.Ticket.Category;
import com.epam.jmp.model.User;
import com.epam.jmp.model.exception.EntityNotFoundException;
import com.epam.jmp.model.impl.EventImpl;
import com.epam.jmp.model.impl.TicketImpl;
import com.epam.jmp.model.impl.UserImpl;
import com.epam.jmp.repository.impl.TicketRepositoryImpl;
import com.epam.jmp.storage.BookingStorage;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(value = "/config/daos.xml")
public class TicketRepositoryTest {

	@Autowired
	TicketRepository repository;
	@Autowired
	SimpleDateFormat formatter;
	
	@Test
	public void shouldReturnNewTicketWhenBookTicket() {
		Ticket ticket = getTicket(0, 1L, 1L, 12, Category.PREMIUM);

		Ticket bookedTicket = repository.bookTicket(ticket.getUserId(), ticket.getEventId(), ticket.getPlace(), ticket.getCategory());
		
		assertTicket(ticket, bookedTicket);
	}
	
	
	@Test
	public void shouldThrowExceptionWhenBookTicketWithInvalidPlace() {
		Ticket ticket = getTicket(0, 1, 1, 0, Category.BAR);
		
		assertThrows(IllegalArgumentException.class, () -> {
			repository.bookTicket(ticket.getUserId(), ticket.getEventId(), ticket.getPlace(), ticket.getCategory());
		});
	}
	
	@Test
	public void shouldThrowExceptionWhenBookTicketWithBookedPlace() {
		
		Ticket ticket = getTicket(0, 1, 1, 23, Category.PREMIUM);
		
		assertThrows(IllegalStateException.class, () -> {
			repository.bookTicket(ticket.getUserId(), ticket.getEventId(), ticket.getPlace(), ticket.getCategory());
		});
	}
	
	@Test
	public void shouldReturnTrueWhenCancelTicket() {
		boolean result = repository.cancelTicket(1L);
		
		assertTrue(result);
	}
	
	@Test
	public void shouldReturnFalseWhenCancelTicket() {
		boolean result = repository.cancelTicket(33L);
		
		assertFalse(result);
	}
	
	@Test
	public void shouldReturnBookedTicketsWhenGetBookedTickets() {
		User user = getUser(1L, "test_user@email.com", "Test User");
		int pageSize = 5;
		int pageNumber = 1;
		
		List<Ticket> tickets = repository.getBookedTickets(user, pageSize, pageNumber);
		
		assertTrue(tickets.size() == 2);
		assertEquals(user.getId(), tickets.get(0).getUserId());
		assertEquals(user.getId(), tickets.get(1).getUserId());
	}
	
	@Test
	public void shouldReturnEmptyListWhenGetBookedTickets() {
		User user = getUser(10L, "tenthUser@email.com", "User Ten");
		int pageSize = 5;
		int pageNumber = 1;
		
		List<Ticket> tickets = repository.getBookedTickets(user, pageSize, pageNumber);
		
		assertTrue(tickets.size() == 0);
	}
	
	@Test
	public void shouldReturnEmptyListWhenGetBookedTicketsWithNextPage() {
		User user = getUser(1L, "test_user@email.com", "Test User");
		int pageSize = 5;
		int pageNumber = 2;
		
		List<Ticket> tickets = repository.getBookedTickets(user, pageSize, pageNumber);
		
		assertTrue(tickets.size() == 0);
	}
	
	@Test
	public void shouldThrowExceptionWhenGetBookedTicketsWithInvalidPageNumber() {
		User user = getUser(1L, "test_user@email.com", "Test User");
		int pageSize = 5;
		int pageNumber = 0;
		
		assertThrows(IllegalArgumentException.class, () -> {
			repository.getBookedTickets(user, pageSize, pageNumber);
		});
	}
	
	@Test
	public void shouldReturnBookedTicketsWhenGetBookedTicketsWithEvent() throws ParseException {
		Event event = getEvent(2L, formatter.parse("12-01-2015 13:15:00"), "Linux:Ubuntu");
		int pageSize= 5;
		int pageNumber = 1;
		
		List<Ticket> tickets =  repository.getBookedTickets(event, pageSize, pageNumber);
		assertTrue(tickets.size() == 1);
	}
	
	@Test
	public void shouldReturnEmptyListWhenGetBookedTicketsWithEvent() throws ParseException {
		Event event = getEvent(9L, formatter.parse("04-08-2021 16:00:00"), "Java: Spring boot 1.0");
		int pageSize = 5;
		int pageNumber = 1;
		
		List<Ticket> tickets = repository.getBookedTickets(event, pageSize, pageNumber);
		
		assertTrue(tickets.size() == 0);
	}
	
	@Test
	public void shouldReturnEmptyListWhenGetBookedTicketsWithEventNextPage() throws ParseException {
		Event event = getEvent(2L, formatter.parse("04-01-2021 16:00:00"), "Java: Spring boot 2.0");
		int pageSize= 5;
		int pageNumber = 2;
		
		List<Ticket> tickets =  repository.getBookedTickets(event, pageSize, pageNumber);
		assertTrue(tickets.size() == 0);
	}
	
	@Test
	public void shouldThrowExceptionWhenGetBookedTicketsWithEventInvalidPageNumber() throws ParseException {
		Event event = getEvent(2L, formatter.parse("04-01-2021 16:00:00"), "Java: Spring boot 2.0");
		int pageSize= 5;
		int pageNumber = 0;
		
		assertThrows(IllegalArgumentException.class, () -> {
			repository.getBookedTickets(event, pageSize, pageNumber);
		});
	}
	
	private Event getEvent(long id, Date date, String title) {
		Event event = new EventImpl();
		event.setId(id);
		event.setDate(date);
		event.setTitle(title);
		
		return event;
	}
	
	private User getUser(long id, String email, String name) {
		User user = new UserImpl();
		user.setId(id);
		user.setEmail(email);
		user.setName(name);
		
		return user;
	}
	

	private Ticket getTicket(long id, long userId, long eventId, int place, Category category) {
		Ticket ticket = new TicketImpl();
		ticket.setId(id);
		ticket.setUserId(userId);
		ticket.setEventId(eventId);
		ticket.setPlace(place);
		ticket.setCategory(category);
		
		return ticket;
	}
	
	private void assertTicket(Ticket actualTicket, Ticket expectedTicket) {
		assertEquals(actualTicket.getEventId(), expectedTicket.getEventId());
		assertEquals(actualTicket.getUserId(), expectedTicket.getUserId());
		assertEquals(actualTicket.getPlace(), expectedTicket.getPlace());
		assertEquals(actualTicket.getCategory(), expectedTicket.getCategory());
	}
}
