package com.epam.jmp.service;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.epam.jmp.model.Event;
import com.epam.jmp.model.Ticket;
import com.epam.jmp.model.Ticket.Category;
import com.epam.jmp.model.exception.EntityNotFoundException;
import com.epam.jmp.model.User;
import com.epam.jmp.model.impl.EventImpl;
import com.epam.jmp.model.impl.TicketImpl;
import com.epam.jmp.model.impl.UserImpl;
import com.epam.jmp.repository.EventRepository;
import com.epam.jmp.repository.TicketRepository;
import com.epam.jmp.repository.UserRepository;
import com.epam.jmp.service.impl.TicketServiceImpl;

@ExtendWith(MockitoExtension.class)
public class TicketServiceTest {
	
	@Mock
	TicketRepository ticketRepository;
	@Mock
	UserRepository userRepository;
	@Mock
	EventRepository eventRepository;
	TicketService ticketService;
	
	@BeforeEach
	public void init() {
		ticketService = new TicketServiceImpl(ticketRepository, eventRepository, userRepository);
	}
	
	@Test
	public void shouldReturnTicketWhenBookTicket() {
		Ticket unbookedTicket = getTicket(0, 1L, 1L, 5, Category.PREMIUM);
		Ticket ticket = getTicket(1L, 1L, 1L, 5, Category.PREMIUM);
		
		when(userRepository.isPresent(unbookedTicket.getUserId())).thenReturn(true);
		when(eventRepository.isPresent(unbookedTicket.getEventId())).thenReturn(true);
		when(ticketRepository.bookTicket(unbookedTicket.getUserId(), unbookedTicket.getEventId(), unbookedTicket.getPlace(), unbookedTicket.getCategory())).thenReturn(ticket);
		
		Ticket result = ticketService.bookTicket(unbookedTicket.getUserId(), unbookedTicket.getEventId(), unbookedTicket.getPlace(), unbookedTicket.getCategory());
		
		assertTicket(ticket, result);
		
	}
	
	@Test
	public void shouldThrowExceptionWhenBookTicketWithInvalidUserId() {
		Ticket unbookedTicket = getTicket(0, 1L, 1L, 5, Category.PREMIUM);
		
		when(userRepository.isPresent(unbookedTicket.getUserId())).thenReturn(false);
		
		assertThrows(EntityNotFoundException.class, () -> {
			ticketService.bookTicket(unbookedTicket.getUserId(), unbookedTicket.getEventId(), unbookedTicket.getPlace(), unbookedTicket.getCategory());
		});
		
	}
	
	@Test
	public void shouldThrowExceptionWhenBookTicketWithInvalidEventId() {
		Ticket unbookedTicket = getTicket(0, 1L, 1L, 5, Category.PREMIUM);
		
		when(userRepository.isPresent(unbookedTicket.getUserId())).thenReturn(true);
		when(eventRepository.isPresent(unbookedTicket.getEventId())).thenReturn(false);
		
		assertThrows(EntityNotFoundException.class, () -> {
			ticketService.bookTicket(unbookedTicket.getUserId(), unbookedTicket.getEventId(), unbookedTicket.getPlace(), unbookedTicket.getCategory());
		});
		
	}
	
	@Test
	public void shouldReturnTrueWhenCancelTicket() {
		long ticketId = 1L;
		when(ticketRepository.cancelTicket(ticketId)).thenReturn(true);
		
		boolean result = ticketService.cancelTicket(ticketId);
		assertTrue(result);
	}
	
	@Test
	public void shouldReturnFalseWhenCancelTicket() {
		long ticketId = 100L;
		when(ticketRepository.cancelTicket(ticketId)).thenReturn(false);
		
		boolean result = ticketService.cancelTicket(ticketId);
		
		assertFalse(result);
	}
	
	@Test
	public void shouldReturnTicketsWhenGetBookedTicketsByUser() {
		User user = new UserImpl();
		user.setId(16L);
		user.setEmail("jock_rechard@email.com");
		user.setName("Jock Rechard");
		
		Ticket ticket1 = getTicket(77L, user.getId(), 56L, 89, Category.STANDARD);
		Ticket ticket2 = getTicket(49L, user.getId(), 98L, 39, Category.STANDARD);
		Ticket ticket3 = getTicket(65L, user.getId(), 5L, 69, Category.BAR);
		Ticket ticket4 = getTicket(15L, user.getId(), 3L, 9, Category.PREMIUM);
		
		int pageSize = 5;
		int pageNumber = 1;
		
		when(ticketRepository.getBookedTickets(user, pageSize, pageNumber)).thenReturn(List.of(ticket4, ticket1, ticket2, ticket3));
		
		List<Ticket> tickets = ticketService.getBookedTicket(user, pageSize, pageNumber);
		assertTrue(tickets.size() == 4);
	}
	
	@Test
	public void shouldReturnTicketsWhenGetBookedTicketsByEvent() {
		Event event = new EventImpl();
		event.setId(9L);
		event.setDate(new Date());
		event.setTitle("Js security hole");
		
		Ticket ticket1 = getTicket(77L, 12L, event.getId(), 123, Category.PREMIUM);
		Ticket ticket2 = getTicket(49L, 96L, event.getId(), 33, Category.STANDARD);
		Ticket ticket3 = getTicket(65L, 5L, event.getId(), 98, Category.BAR);
		
		int pageSize = 3;
		int pageNumber = 6;
		when(ticketRepository.getBookedTickets(event, pageSize, pageNumber)).thenReturn(List.of(ticket3, ticket2, ticket1));
		
		List<Ticket> tickets = ticketService.getBookedTicket(event, pageSize, pageNumber);
		
		assertTrue(tickets.size() == 3);
		
	}
	
	private void assertTicket(Ticket actualTicket, Ticket expectedTicket) {
		assertEquals(actualTicket.getId(), expectedTicket.getId());
		assertEquals(actualTicket.getEventId(), expectedTicket.getEventId());
		assertEquals(actualTicket.getUserId(), expectedTicket.getUserId());
		assertEquals(actualTicket.getCategory(), expectedTicket.getCategory());
		assertEquals(actualTicket.getPlace(), expectedTicket.getPlace());
	}
	
	private Ticket getTicket(long id, long userId, long eventId, int place, Category category) {
		Ticket ticket = new TicketImpl();
		ticket.setId(id);
		ticket.setEventId(eventId);
		ticket.setUserId(userId);
		ticket.setPlace(place);
		ticket.setCategory(category);
		
		return ticket;
	}
}
